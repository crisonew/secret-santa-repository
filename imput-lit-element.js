import {LitElement, html, css } from 'lit';
import '@material/mwc-button';
import '@material/mwc-list';
import '@material/mwc-list/mwc-check-list-item';
import '@material/mwc-checkbox';
import '@material/mwc-textfield'

class InputElement extends LitElement{
    static get properties()
    {
        return {
            participante : { type: String}
        };
    }

    constructor()
    {
        super();
        this.participante='';
    }

    
    static get styles()
    {
        return css `
        mwc-button{
            width: 100%;
            --mdc-theme-primary: #fc9803;
            --mdc-theme-on-primary: white;
            margin-bottom: 10px;
        }
        mwc-textfield{
            width: 100%;
            alignt:center;
            --mdc-theme-primary: #fc9803;

        }
        `;
    }

    updated()
    {
        console.log("Input-lit elemento actualizado")
    }

    accionButton(e)
    {
        this.participante = this.shadowRoot.querySelector('#inputAgregarParticipante').value;
        this.dispatchEvent(
            new CustomEvent('on-change', { detail: { value: this.participante } })
          );
        this.shadowRoot.querySelector('#inputAgregarParticipante').value='';
    }


    render(){
    return html`
    <mwc-textfield filled id="inputAgregarParticipante" label="Participante"></mwc-textfield>
    <mwc-button raised id="agregarParticipante" @click=${e => this.accionButton(e)}>Agregar</mwc-button>
    `;
    }
}

customElements.define('input-lit-element', InputElement);