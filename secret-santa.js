import {LitElement, html, css } from 'lit';
import '@material/mwc-button';
import '@material/mwc-list';
import '@material/mwc-list/mwc-check-list-item';
import '@material/mwc-checkbox';
import '@material/mwc-top-app-bar-fixed';
import '@material/mwc-icon-button';
import './imput-lit-element.js';
import './participante-lit-element.js';

class SecretSanta extends LitElement{
    static get properties()
    {
        return{
            elementos : {type : String},
            nombres : { type: Array},
            nombre : { type: String}
        };
    }

    constructor()
    {
        super();
        this.elementos = [];
        this.nombres = [];
    }

    static get styles()
    {
        return css `
        mwc-button{
            width: 100%;
            --mdc-theme-primary: #fc9803;
            --mdc-theme-on-primary: white;
        }
        mwc-top-app-bar-fixed {
            --mdc-theme-primary: orange;
            --mdc-theme-on-primary: black;
          }
        #container{
            width: 80%;
            margin: auto;
        }
          
        `;
    }



    addParticipante(e)
    {
        console.log("estoy dandole click");
        const nombreActual = this.shadowRoot.querySelector('#inputParticipante').participante;

        if(!(nombreActual === '')){
        console.log("El nombre actual es " + nombreActual);
        this.nombres = [ ...this.nombres, nombreActual];
        console.log(this.nombres);
        }
    }

    hacerSorteo(e)
    {
        //algoritmo del sorteo;
    }

    render(){
    return html`
        <mwc-top-app-bar-fixed centerTitle>
            <mwc-icon-button icon="menu" slot="navigationIcon"></mwc-icon-button>
            <div slot="title">Secret Santa</div>
            <mwc-icon-button icon="inventory" slot="actionItems"></mwc-icon-button>
            <div><!-- content --></div>
        </mwc-top-app-bar-fixed>

        <div id="container">
            <input-lit-element id="inputParticipante" @on-change="${(this.addParticipante)}"></input-lit-element>
            
            ${this.nombres.length=== 0 ? 
                html`Añada elementos por favor`
            : html`<mwc-list multi>
            ${this.nombres.map(
                (nombre) => html`
                <div><mwc-icon>insert_emoticon</mwc-icon>
                    ${nombre}</div>
                    <participante-lit-element id=${nombre}>
                    <slot>${nombre}</slot>
                    </participante-lit-element>
                    <li divider role="separator"></li>
                `
                )}
            
            </mwc-list>
            `}



            ${this.nombres.length > 2 ?
                html`<mwc-button raised id="hacerSorter" @click="${this.hacerSorteo}">HACER SORTEO</mwc-button>`:
                html`No hay suficientes participantes`
            }
            

            
        </div>
        `;
    }
}

customElements.define('secret-santa', SecretSanta);