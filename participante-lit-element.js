import {LitElement, html, css } from 'lit';
import '@material/mwc-button';
import '@material/mwc-list';
import '@material/mwc-list/mwc-check-list-item';
import '@material/mwc-checkbox';
import '@material/mwc-textfield'

class ParticipanteElement extends LitElement{
    static get properties()
    {
        return {
            nombre : { type: String},
            deseos: { type : Array}
        };
    }

    constructor()
    {
        super();
        this.participante='';
        this.deseos = [];
    }

    
    static get styles()
    {
        return css `
        mwc-button{
            width: 100%;
            --mdc-theme-primary: #fc9803;
            --mdc-theme-on-primary: white;
        }
        mwc-textfield{
            --mdc-theme-primary: #fc9803;
            width: 100%;
        }
        div{
            text-align: left;
            margin: 10px;
        }
        `;
    }

    addRegalos()
    {
        const elemento = this.shadowRoot.querySelector('#inputRegalo').value;
        this.dispatchEvent(
            new CustomEvent('on-change', { detail: { value: elemento } })
          );
        this.deseos = [...this.deseos, elemento];

    }

    deleteDeseo(e)
    {
        this.deseos = this.deseos.filter((deseo) => deseo !== e.target.id);

    }

    render(){
    return html`
    <mwc-textfield filled id="inputRegalo"label="Regalo"></mwc-textfield>
    <mwc-button raised @click="${e => this.addRegalos(e)}">Agregar</mwc-button>
    
    ${this.deseos.length=== 0 ? 
        html`Añada elementos por favor`
      : html`<mwc-list multi>
      ${this.deseos.map(
          (deseo) => html`
            <mwc-list-item graphic="icon">
              <slot>${deseo}</slot>
              <mwc-button id=${deseo} @click=${this.deleteDeseo} slot="graphic">X</mwc-button>
            </mwc-list-item>
            <li divider role="separator"></li>
          `
        )}
      
      </mwc-list>
      `}

    `;
    }
}

customElements.define('participante-lit-element', ParticipanteElement);